# Questions for Shiny-based Questionnaire

- [ ] proper way of dynamic UI generation
- [ ] reactivity to (sub)set of generated UI items
	- [x] update plot
	- [ ] test for completeness
- [ ] report generation (`render*`)
	- [ ] printing
- 
