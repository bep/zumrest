# ZUMREST

Questionnaire Samy Egli

## Issues for Shiny-based Questionnaire

### For Demo
- [x] reactivity to (sub)set of generated UI items
	- [x] update plot
	- [ ] test for completeness
	    - temporary solution: check completeness on submit, warn if incomplete 
- [x] CSV
    - [x] create unique ID for subject  
    - [x] save results for later analysis (CSV)
- [x] report generation (`render*`)
	- [ ] printing
	    - print from PDF-Viewer 

### For later
- [ ] proper way of dynamic UI generation
- [ ] admin interface
    - [ ] overview
    - [ ] individual analysis
    - [ ] summary analysis?