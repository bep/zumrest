require('readxl')
require(lubridate)

#' extract from string based on regular expression pattern
#'
#' @param x string (or vector thereoof)
#' @param pattern rergular expression to match in \code{x}
#' @param ... pass to \code{\link[base]{gregexpr}} and \code{\link[base]{regexec}}
#'
#' @return vector (or matrix) of matches
#' @export
#'
#' @examples
regextract <- function(x,
                       pattern,
                       ...){
    n <- length(x)
    return(matrix(unlist(sapply(regmatches(x,
                                           gregexpr(pattern, x, ...)),
                                function(e) regmatches(e, regexec(pattern, e, ...)))),
                  nrow = n,
                  byrow = TRUE))

}

#' get extension from filename
#'
#' The extension is simply found by looking for the last '.' with text behind.
#' Everything starting from that '.' is considered extension and returned.
#'
#' CAVEAT: Bad things may happen when there is no '.' in \code{file} - or more
#' generally, whhen \code{ext.pattern} is not found. Hopefully caught with
#' empty  string returned for such cases
#'
#' @param file filename or vector of filenames
#' @param ext.pattern pattern used for detection using '[^\\.]+' will work
#' @param ... not used
#'
#' @return \eqn{n\times 1}{nx1} matrix with extensions (including the '.')
#' @export
#'
#' @examples
#' get.extension(c('file.xl', 'datei.txt', 'bad', 'good.csv'))
get.extension <- function(file,
                          ext.pattern = '\\.[^\\.]+',
                          ...){
    file <- basename(file)
    good <- grepl(ext.pattern, file)
    ext <- character(length(file))
    ext[good] <- regextract(file,
                            pattern = ext.pattern)[1:sum(good)]
    return(ext)
}

##' .. content for \description{} (no empty lines) ..
##'
##' .. content for \details{} ..
##' @title Read questions from CVS
##' @param csv input file
##' @param ... currently ignored
##' @return data.frame with questions and options
##' @author Benno Pütz \email{puetz@@psych.mpg.de}
read.questions <- function(file    = '~/Work/4samy/RessourcenStressorenBalance.xlsx',
                           sep    = '\t',
                           header = FALSE,
                           ...){
    switch(get.extension(file),
           '.xlsx'={
               questions <- as.data.frame(read_excel(file))
           },
           '.csv'=,
           '.txt'= {
               questions <- read.csv(csv,
                                     sep        = sep,
                                     fill       = TRUE,
                                     header     = header,
                                     as.is      = TRUE,
                                     na.strings = '')
               col.names <- c('part',
                              'index',
                              'question',
                              'type',
                              'first.index',
                              'options',
                              'area',
                              'category')
               colnames(questions) <- col.names

           }
    )
    return(within(questions,
                  id <-  paste0(part,'.',index)
                  )
           )
}


##' execute code generated at run-time
##'
##' The code snippets are pasted to form one long string of code that
##' is then parsed  and executed.
##' It should, thus, be syntactically correct R code.
##'
##' @title evaluate code string
##' @param ... code snippets
##' @return none
##' @author Benno Pütz \email{puetz@@psych.mpg.de}
eptp <- function(..., verbose = FALSE){
    cmd <- paste(...)
    if(verbose){
        message(cmd,"\n")
    }
    return(eval(parse(text=cmd)))
}

##' convenience function
##' Split the option field from the questionaire list.
##' Text fields use '|' as separator, numeric/slider fields use '-' or ':'.
##' For the latter \code{fixed=TRUE} needs to be set.
##'
##' @title split option string
##' @param option.string string with options
##' @param type type of option
##' @param ... passed to \code{strsplit}
##' @return options as character vector
##' @author Benno Pütz \email{puetz@@psych.mpg.de}
choice.opts <- function(option.string,
                        type,
                        ...){
    split <- switch(type,
                    c= '|',
                    n=,
                    s= '[-:]')
    fixed <- switch(type,
                    c= TRUE,
                    n=,
                    s= FALSE)
    return(strsplit(option.string,
                    split,
                    fixed = fixed,
                    perl = !fixed,
                    ...)[[1]])
}

intro <- function(part,
                         ...){
    switch(as.character(part),
           '2'={
               # Describe the duration of consideration for the questions
               #
               # offset by one so we don't need to worry about previous month quirks
               mdays <- c(31, 31,ifelse(leap_year(today()),29,28),31,30,31,30,31,31,30,31,30)
               duration <-  ddays(mdays[month(today())] - 1) # use days for previous month here
               start.day <- today() - duration

               msd <- month(start.day)
               months.german <- c('Januar',
                                  'Februar',
                                  'März',
                                  'April',
                                  'Mai',
                                  'Juni',
                                  'Juli',
                                  'August',
                                  'September',
                                  'Oktober',
                                  'November',
                                  'Dezember')
               return(paste0(
                   'p("Die folgenden Fragen beziehen sich auf den letzten Monat, also den Zeitraum vom ',
                   day(start.day), '. ', months.german[msd],
                   ifelse(msd==12,
                          paste0(' ', year(start.day)),
                          ''),
                   ' bis heute."),'))
           },
           ""
    )
}

##' Programmatically create head (constant part) of tabItem command
##'
##' see \code{\link{full.cmd}}
##' @title create command head
##' @param part part of the questionnaire
##' @param title title for item
##' @param ... currently ignored
##' @return beginning of command string
##' @author Benno Pütz \email{puetz@@psych.mpg.de}
cmd.head <- function(part, title, ...){

    return(paste0("tabItem(tabName = 'part",part,"',",
                 "  titlePanel('",title,"'),",
                 intro(part)
                 )
           )
}

##' Programmatically create body (repeating part) of tabItem command
##'
##' Iterate through all the questions relevant for \code{part} and
##' add the corresponding code to the command's body.
##'
##' See \code{\link{full.cmd}}
##' @title create command body
##' @param part  part of the questionnaire, determines relevant questions from \code{ql}
##' @param ql question list (read with \code{\link{read.questions}})
##' @param random wether to randomize order of questions
##' @param ... currently ignored
##' @return string of looped part of command
##' @author Benno Pütz \email{puetz@@psych.mpg.de}
cmd.body <- function(part, ql, random = FALSE, ...){
    cb <- ''
    part.indices <- which(ql$part == part)
    if (random){
        part.indices <- sample(part.indices)
    }
    for (i in part.indices){
        id.part <- paste0("'", ql[i, 'id'], "',",
                          "label='", ql[i, 'question'], "',")
        message(id.part)
        choices <- choice.opts(ql[i, 'options'],
                               ql[i, 'type'])
        cb <- paste0(cb,
                     'box(',
                     switch(ql[i, 'type'],
                            # numeric
                            n = {
                                # message('numeric')

                                min <- choices[1]
                                max <- choices[2]
                                paste0("numericInput(",
                                       id.part,
                                       "value = NA,",
                                       "min = ", min, ',',
                                       "max = ", max, ')'
                                       )
                            },
                            # slider
                            s = {
                                # message('slider')
                                min <- choices[1]
                                max <- choices[2]
                                step <- ifelse(length(choices)>2, choices[3], 1)
                                paste0("sliderInput(",
                                       id.part,
                                       "value = ", ql[i, 'first.index'], ',',
                                       "min = ",   min,  ',',
                                       "max = ",   max,  ',',
                                       "step = ",  step, ')'
                                )

                            },
                            # choice
                            c = {
                                #message('choice')
                                #message(paste(colnames(ql), collapse = '\n'))
                                #browser()
                                # message(paste(choices, collapse = ' * '))
                                values <- seq(ql[i, 'first.index'],
                                              length.out = length(choices))
                                names(values) <- choices
                                paste0("radioButtons(",
                                       id.part,
                                       "choiceNames = c('",
                                       paste0(choices, collapse="','"),
                                       "'),",
                                       "choiceValues= c('",
                                       paste0(values,  collapse="','"),
                                       "'),",
                                       "width='100%',",
                                       "inline=TRUE,",
                                       "selected=character(0))"
                                )

                            }),
                     ",width=12)",
                     ifelse(i != tail(part.indices, 1),
                            ",",
                            '')   # no trailing comma as tail may be empty
                     )
    }

    return(cb)
}

part.complete <- function(input,
                          part,
                          ...){
    message('part = ', part)
    ids <- unclass(t(ql[ql$part == part, 'id']))
    #browser()
    #message(ids)
    untouched <- sapply(ids, function(id)(is.null(input[[id]])))
    #message("untouched:", untouched)
    message(100*(1-sum(untouched)/length(untouched)),'% done')
    return(!any(untouched))
}

##' Programmatically create tail (mostly constant part) of tabItem command
##'
##' See \code{\link{full.cmd}}
##' @title command tail
##' @param part  part of the questionnaire
##' @param submit Label for submit button (empty for none)
##' @param ... currently ignored
##' @return tail part of command
##' @author Benno Pütz \email{puetz@@psych.mpg.de}
cmd.tail <- function(part,
                     submit = "Weiter",
                     ...){

    ct <- paste(
        if(submit != ''){

            paste0("conditionalPanel(condition = 'part.complete(ql,", part, ")',",
                   "actionButton('submit",part,"','", submit, "'))")

        }
    )

    return(paste0(ifelse(nchar(ct)>0, ",", ''),
                 ct,
                 ")"))
}
##' Construct \code{tabItem} from \code{part} and question list (\code{ql})
##'
##' The parts of the command are set up, concatenated, and executed
##' @title create and execute full command
##'
##' @param part part of the questionnaire
##' @param ql question list (read with \code{\link{read.questions}})
##' @param title title for tab
##' @param submit text for submit button ('' for none)
##' @param random whethher to randomize order of questions
##' @param verbose debugging feedback
##' @param ... simply passed on
##'
##' @return generated tabItem
##' @author Benno Pütz \email{puetz@@psych.mpg.de}
makeTabItem <- function(part,
                        ql,
                        title,
                        intro = NULL,
                        submit = 'Weiter',
                        random = FALSE,
                        verbose = FALSE,
                        ...) {
    eptp(cmd.head(part, title, intro, ...),
         cmd.body(part, ql, ..., random = random),
         cmd.tail(part, submit = submit, ...),
         ...,
         verbose = verbose)
}

dbg.msg <- function(...){
    cat(file = stderr(),
        ...,
        "\n")
}

collect.responses <- function(input,
                              ql,
                              part = 2,
                              ...){
    sub <- ql[ql$part == part, c('first.index', 'scale', 'category')]
    #browser()
    summary <- tapply(sample(0:4, nrow(sub), TRUE), #sub$first.index,
                      sub[,2:3],
                      mean,
                      na.rm = TRUE)
    return(summary)

}


#' Spider plot
#'
#' @param input Shiny's input structure
#' @param ql question list
#' @param ...
#'
#' @return none (plot)
#' @export
#'
#' @examples
response.plot <- function(input,
                          ql,
                          ...){
    resp <- collect.responses(input, ql)
    browser()
    labels <- rownames(resp)

    cbPalette <- c("#999999", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7")

    colors_border <- c( rgb(0.34, 0.71, 0.91, 0.9),  # light blue
                        rgb(0.90, 0.62, 0   , 0.9) ) # orange

    radarchart(data.frame(t(cbind(4,0,resp))),
               axistype    = 1 ,
               # custom polygon
               pcol        = colors_border ,
               pfcol       = colors_in <- adjustcolor(colors_border, 0.4),
               plwd        = 4 ,
               plty        = 1,
               # custom grid
               cglcol      = "grey",
               cglty       = 1,
               axislabcol  = "grey",
               caxislabels = seq(0,4,1),
               cglwd       = 0.8,
               # custom labels
               vlabels     = labels,
               vlcex       = 0.8
    )
    legend(x = 0.62, y = 1.4,
           legend   = colnames(resp),
           bty      = "n",
           pch      = 20 ,
           col      = colors_in ,
           text.col = "grey",
           cex      = 1.2,
           pt.cex   = 3)
}


#' PSS plot
#'
#' to be completed
#'
#' @return
#' @export
#'
#' @examples
pss.plot <- function(){
    ggplot(pss_summary, aes(x = pss.scale, y = pss.score, group = 1)) +
        geom_histogram(stat = "identity", alpha = 1,
                       aes(fill = factor(pss.scale))) +
        scale_y_continuous(expand = c(0, 0), breaks = 0:4) +
        scale_fill_manual(values = cbPalette[c(3, 2)]) +
        coord_cartesian(ylim = c(0, 4)) +
        labs(x = "Mittelwerte") +
        theme_bw() +
        theme(panel.border = element_blank(),
              axis.line = element_line(color="black", size = 0.5),
              legend.position = "none",
              #legend.position = c(0.2, 0.8), # use if desired
              #legend.background = element_rect(colour ="gray90", size=.5, linetype="solid"), # use if desired
              axis.title = element_blank())
    }
