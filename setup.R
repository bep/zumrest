#!/bin/env Rscript
pkg.list <- c("shiny", "shinydashboard", "shinyalert", "shinyjs", "readxl", "lubridate", "fmsb", "ggplot2")
#
need <- character(0)
for(pkg in pkg.list){
    if (!require(package = pkg, quietly  = FALSE, character.only = TRUE)){
        need <- c(need, pkg)
    }

}
if(l <- length(need)){
    warning("Package", ifelse(l>1,'s',''),' are missing, will try to install them\n')
    install.packages(need)
} else {
    message('\n\tLooks OK ;-)\n')
}
